# Daily Leetcode Challenge Todoist Task Maker

Sync daily leetcode challenges to your todoist account. Inspired by [How to Keep Track of Daily LeetCode Challenges With Todoist and Cloudflare Worker](https://hackernoon.com/how-i-sync-daily-leetcoding-challenge-to-todoist?source=rss), but made in Python.

## Prerequisites

Python v3.x

## Project setup

1. Run command `pip install -r requirements.txt'
2. Rename `.env-example` file to `.env`
3. Add your Todoist API token to the `TODOIST_API_TOKEN` variable. See [Documentation(https://developer.todoist.com/rest/v1/#overview) for details

## License

[View license](LICENSE) for copyright details on using the code in this project.


