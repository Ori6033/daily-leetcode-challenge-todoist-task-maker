import requests
import todoist
import os
import json
from collections import namedtuple
from dotenv import load_dotenv
from string import Template

load_dotenv()

# Retrieve daily leetcode challenges from their API with a graphQL query
def get_daily_leetcode_challenge():
    url = 'https://leetcode.com/graphql'
    query = { 'query': '''
    query questionOfToday {
        activeDailyCodingChallengeQuestion {
            date
            userStatus
            link
            question {
                acRate
                difficulty
                freqBar
                frontendQuestionId: questionFrontendId
                isFavor
                paidOnly: isPaidOnly
                status
                title
                titleSlug
                hasVideoSolution
                hasSolution
                topicTags {
                    name
                    id
                    slug
                }
            }
        }
    }
    '''}

    print('Fetching daily coding challenge from LeetCode API.')
    response = requests.post(url, json = query)
    return response.text

# Add task to inbox project
def add_todoist_task(content, description, due_string, priority):
    TODOIST_API_TOKEN = os.getenv('TODOIST_API_TOKEN')
    api = todoist.TodoistAPI(TODOIST_API_TOKEN)
    api.sync()

    print(Template('Creating todoist task $content').substitute(content=content))
    api.items.add(
        content,
        project_id = api.state["user"]["inbox_project"],
        description=description,
        due={'string': due_string},
        priority=priority
    )
    api.commit()
    print(Template('Todoist task $content created').substitute(content=content))

# Decode json string to a python object
def objectDecoder(objectDict):
    return namedtuple('X', objectDict.keys())(*objectDict.values())

challenge = json.loads(get_daily_leetcode_challenge(), object_hook=objectDecoder).data.activeDailyCodingChallengeQuestion

add_todoist_task(
    Template('[$title](https://leetcode.com$link)')
        .substitute(title=challenge.question.title, link=challenge.link),
    Template('Difficulty: $difficulty')
        .substitute(difficulty=challenge.question.difficulty),
    'Today',
    4
)
